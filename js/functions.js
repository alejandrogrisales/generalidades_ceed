var actual = 0;
var dp_actual = 1;
var titulo = "Generalidades<br/> CEED";
var menu = ["Bienvenida", "Presentaci&oacute;n", "El papel del DANE en el pa&iacute;s", "Que es el CEED", "Universo de estudio", "Cobertura geogr&aacute;fica", "Miembros del equipo operativo", "Conclusiones"];
var hijos = [1, 2, 2, 2, 7, 1, 3, 2];
var total_hijos = 0;

$('#click').click(function(event) {
    event.preventDefault();
    $('.container').toggleClass('pushed');
});

function progress(percent, $element) {
    var progressBarWidth = percent * $element.width() / 100;
    $element.find('div').animate({width: progressBarWidth}, 500)/*.html(percent + "% ")*/;
}

function verificar_fls() {

    if ((actual == (menu.length - 1) && dp_actual == (hijos[actual])))
        $("#right_ln").hide();
    else
        $("#right_ln").show();

    if ((actual == 0 && (dp_actual == 1 || dp_actual == 0)))
        $("#left_ln").hide();
    else
        $("#left_ln").show();
}
function fn_cargarrecursos(idContenido, idHijo) {
    var carpeta = "t" + idContenido;
    var html = "_" + idHijo + ".html";
    var porcentaje;
    actual = idContenido;
    dp_actual = idHijo;

    verificar_fls();
    $.ajaxSetup({
        beforeSend: function(xhr, status) {
            // TODO: show spinner
            $('#spinner').show();
        },
        complete: function() {
            // TODO: hide spinner
            $('#spinner').hide();
        }
    });
    $('#contenido').load('contents/' + carpeta + "/" + carpeta + html + '?=' + (new Date()).getTime());
    porcentaje = obtener_porcentaje();
    progress(porcentaje, $('#progressBar'));
}
function obtener_porcentaje() {
    var rel = fn_total_hijos(actual)+ dp_actual;
    return Math.ceil((rel*100)/total_hijos);
    
}
function fn_total_hijos(ult_elem) {
    var total_ret = 0;
    $.each(hijos, function(k, v) {
        if (k < ult_elem)
            total_ret += v;
        
    });
   
    return total_ret;
    
}
function fn_generarmenu() {

    total_hijos = fn_total_hijos(menu.length);
    $.each(menu, function(k, v) {
        if (k == 0) {
            $("#cssmenu ul").append('<li><a href="index.html"><span class="tab">' + v + '</span></a></li>');
        } else {
            $("#cssmenu ul").append('<li><a href="javascript:fn_cargarrecursos(' + k + ',1)"><span class="tab">' + v + '</span></a></li>');
        }
//        alert( "Key: " + k + ", Value: " + v );
    });
    
     
    /*
     $.each(menu, function(index,val) {
     $("#myList").append(
     $("<li>", {}).append(
     $('<a>').attr('href','/user/messages'+index).append(
     $('<span>').append(val)
     )
     )
     );
     });
     */
}

$(function() {
    $('#left_ln').click(function(event) {

        dp_actual--;
        if (dp_actual == 0) {
            actual--;
            dp_actual = hijos[actual];
        }
        fn_cargarrecursos(actual, dp_actual);
    });

    $('#right_ln').click(function(event) {

        dp_actual++;
        if (dp_actual > hijos[actual]) {
            dp_actual = 1;
            actual++;
        }
        fn_cargarrecursos(actual, dp_actual);
    });
    fn_generarmenu();
    verificar_fls();
    fn_cargarrecursos(actual, dp_actual);
   
});

<!--ACCIONES--->
function escribe () {
	$('#tit').html(titulo);
	$('#subTit').html(menu[actual]);
}

<!--BOTONERA-->
function btn() {
	$("#modal" ).hide();
	var n = $("#botones li").size();
	for (var i=0; i<n; i++) {
		var h = i+1;
		var btnClk = "#b"+h;

		$(btnClk).click(function(){
			selec = $(this).attr("id");
			url = $(this).attr("name");
			cargaCont(selec,url);
			
			$(this).addClass("visto");
		});
	}	
}

<!---MODAL--->
				
function cargaCont(selec,url) {
	var pagina = selec.slice(-1);
	var vinculo = url;
	var itm = vinculo+pagina+".html"

	$("#modal").fadeTo("fast",0, function() {
		$("#contModal").load(itm);
		$("#modal").fadeTo("fast",1);
	});
};

function cerrar() {
	$("#modal").fadeTo("fast",0, function() {
		$("#modal").hide();
	});
}
		
function cambiaTxt(selec) {
	var itm = eval(selec); 
	$("#contenidosIn").fadeTo("fast",0, function() {
	$("#contenidosIn").html(itm);
	$("#contenidosIn").fadeTo("fast",1);
	});
};

<!---RETRO--->	

function ocultaRetro () {
	$("#mal").hide();
	$("#bien").hide();
}
	
function errado() {
	$("#mal").fadeTo("fast",1);
}

function acierta() {
	$("#bien").fadeTo("fast",1);
}

function cierraVentana (selec) {
	var obj = "#"+selec;
	$(obj).fadeTo("fast",0, function() {
		$(obj).hide();
	});
}

<!---ACORDEON---->
function acord() {
	$('.acordeon').magicAccordion({
		headingTag 	: 'h2',
		bodyClass 	: 'body',
		headClass 	: 'head',
		activeClass : 'active',
		speed 		: 200,
		easing 		: 'swing',
		openOnLoad 	: 0,
		hashTrail : false
	})
	.on( 'opened.magic', function(e){
		console.log(e.head);
	})
	.on( 'closed.magic', function(e){
		console.log(e.body);
	});

	var app = $('.acordeon').data( 'magic-accordion' );
}

<!---SELECTOR--->
function selector () {
	$("span").css({"color": "rgba(255,255,255,0)"});
	
	$("li").click(function(){
		var seleccionado = $(this).attr("id");
		var sel = eval('"#'+seleccionado+' > span"')
		$(sel).css({"color": "#fff"});
	});
}